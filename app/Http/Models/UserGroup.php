<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'user_to_group';
    
    protected $fillable = ['user_id', 'group_id'];
    
    public $timestamps = false;
}
