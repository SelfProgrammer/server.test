<?php

namespace App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Group;
use App\Http\Models\UserGroup;
use App\Http\Requests\GroupRequest;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Group::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\GroupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $model = Group::create($request->all());
        return $model;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return $group;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\GroupRequest  $request
     * @param  \App\Http\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, Group $group)
    {
        $group->fill($request->all());
        $group->save();
        return response()->json($group);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        if($group->delete()) {
            return response(null, 204);
        }
    }

    /**
     * Remove/Add the specified user from/to group.
     *
     * @param  \App\Http\Models\Group  $group
     * @param  \App\Http\Models\User  $group
     * @return \Illuminate\Http\Response
     */
    public function users(Group $group, User $user)
    {
        $model = UserGroup::whereUserId($user->id)->whereGroupId($group->id)->first();
        if ($model) {
            $model->delete();
//            return group users
            return 'deleted';
        }
        
        $model = new UserGroup;
        $model->user_id = $user->id;
        $model->group_id = $group->id;
        $model->save();
//            return group users
            return 'added';
    }
}
