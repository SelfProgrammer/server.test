<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserToGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_to_group', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('group_id')->unsigned();
            
            $table->foreign('user_id')->references('id')->on('users')
                ->index('user_to_group_user_id_users_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->foreign('group_id')->references('id')->on('groups')
                ->index('user_to_group_group_id_groups_id')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_to_group', function (Blueprint $table) {
            $table->dropForeign('user_to_group_user_id_users_id');
            $table->dropForeign('user_to_group_group_id_groups_id');
        });
        Schema::dropIfExists('user_to_group');
    }
}
