<?php

use Illuminate\Http\Request;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('/groups', 'GroupController');
Route::put('groups/{group}/users/{user}', ['uses' => 'GroupController@users', 'as' => 'groups.users']);

Route::apiResource('/users', 'UserController');
